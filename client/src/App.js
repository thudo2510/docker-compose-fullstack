import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            apiResponse: "",
            dbResponse: "", 
            users: [],
            user: {
                name: '',
                id: ''
            }
        };
    }

    // Go to API and check testAPI route for a response
    callAPI() {
        fetch("http://localhost:9000/testAPI")
            .then(res => res.text())
            .then(res => this.setState({ apiResponse: res }))
            .catch(err => err);
    };

    // Go to API and check testDB route for a response
    callDB() {
        fetch("http://localhost:9000/testDB")
            .then(res => res.text())
            .then(res => this.setState({ dbResponse: res }))
            .catch(err => err);
    };

    // get users
    getUser() {
        fetch("http://localhost:9000/users")
            .then(res => res.json())
            .then(res => this.setState({ users: res }))
            .catch(err => err);
    };
    
    createUser(name) {
        fetch("http://localhost:9000/users/create", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name
            })
        })
            .then(res => res.text())
            .then(res => this.getUser())
            .catch(err => err);
    };

    deleteUser(id) {
        console.log('del id', id);
        fetch("http://localhost:9000/users/delete", {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id
            })
        })
            .then(res => res.text())
            .then(res => this.getUser())
            .catch(err => err);
    };

    updateUser(user) {
        fetch("http://localhost:9000/users/update", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
        })
            .then(res => res.text())
            .then(res => this.getUser())
            .catch(err => err);
    };

    // Execute the calls when componnent mounts
    componentDidMount() {
        this.callAPI();
        //this.createUser('hello');
        this.getUser();
    }

    render() {
        
        const handleNameChange = (event) => {
            var value = event.target.value;
            this.setState(prevState => ({ user: { ...prevState.user, name: value } }));
        };

        const handleSave = () => {
            var user = this.state.user;
            if(user.name) {
                user.id ? this.updateUser(user): this.createUser(user.name);
                this.setState({ user: { name: '', id: '' } });
            }
        }

        const handleDelete = user => {
            console.log('user', user);
            if(user) {
                this.deleteUser(user._id);
            }
        }

        const handleSelectUser = user => {
            this.setState({ user: { name: user.name, id: user._id} });
            console.log(this.state.user);
        }

        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1 className="App-title">Welcome to React</h1>
                </header>
                <p className="App-intro">{this.state.apiResponse}</p>
                <p className="App-intro">{this.state.dbResponse}</p>
                <div>
                    <label htmlFor="name">Name: </label>
                    <input type="text" name="name" id="name" value={ this.state.user.name } onChange={ event => handleNameChange(event) }/>
                    <button onClick={ handleSave }>{ this.state.user.id ? 'Update' : 'Add' }</button>
                    { this.state.user.id && <button onClick={ () => handleSelectUser({ name: '', id: '' }) }>Cancel</button> }
                </div>
                <div>
                    <ul style={{ display: 'inline-block' }}>
                        {
                            this.state.users &&
                            this.state.users.map(user => (
                                <li key={ user._id } style={ user._id === this.state.user.id ? { color: 'red' } : null }>
                                    <div style={{ display: 'flex' }}>
                                        <span style={{ flexGrow: 1 }} onClick={ () => handleSelectUser(user) }>{ user.name }</span>
                                        <button style={{ flexGrow: 0, margin: '0 1em' }} onClick={ () => handleDelete(user) }>x</button>
                                    </div>
                                </li>
                            ))
                        }
                    </ul>
                </div>
            </div>
        );
    }
}

export default App;
