const mongoose = require('mongoose');

//runing on docker localhost => host.docker.internal
const connection = 'mongodb://host.docker.internal:27017/mongo-test';

const connectDb = () => {
    return mongoose.connect(connection);
};

module.exports = connectDb;