var express = require('express');
var router = express.Router();
var User = require('../src/user.model');

/* GET users listing. */
router.get('/', async (req, res, next) => {
  const users = await User.find();
  res.json(users);
  //res.send('respond with a resource');
});

router.post('/create', async (req, res, next) => {
  const { name } = req.body;

  const existedUser = await User.find({ name }).exec();
  if(existedUser && existedUser.length > 0) {
    res.send('username existed');
  } else {
    const user = new User({ name });
    await user.save().then(() => console.log('create user:', user));
    res.send('created user success');
  }
});

router.delete('/delete', async (req, res, next) => {
  const { id } = req.body;
  await User.remove({ _id: id}).exec();
  console.log('delete ', req.body);

  res.send('delete user success');
});

router.post('/update', async (req, res, next) => {
  const { id, name } = req.body;

  const existedUser = await User.find({ name }).exec();
  if(existedUser && existedUser.length > 0) {
    res.send('username existed');
  } else {
    await User.update({ _id: id}, { name }, {upsert: true}, error => res.send('update user success'));
    console.log('update ', req.body);

    res.send('update user success');
  }
});

module.exports = router;
